package com.agileengine.crawler.impl;

import com.agileengine.context.Context;
import com.agileengine.crawler.Crawler;
import com.agileengine.util.impl.CSSSelectorBuilderImpl;
import com.agileengine.util.impl.PathBuilderImpl;
import com.google.common.collect.Iterables;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

@Slf4j
public class CrawlerImpl implements Crawler {

  private static final String CHARSET_NAME = "utf8";

  @Override
  public void findElementPath(Context context) {
    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    Validator validator = factory.getValidator();
    Set<ConstraintViolation<Context>> constraintViolations = validator.validate(context);
    if (constraintViolations.isEmpty()) {
      Optional<Document> sourceDocumentOpt = getDocument(new File(context.getSourceDocument()));
      sourceDocumentOpt.ifPresent(sourceDocument -> {
        Optional<Element> sourceElementOpt = findElementById(sourceDocument,
            context.getElementId());
        sourceElementOpt.ifPresent(
            element -> {
              Optional<Document> targetDocumentOpt = getDocument(
                  new File(context.getTargetDocument()));
              targetDocumentOpt.ifPresent(targetDocument -> {
                findElementInTargetDocument(element, targetDocument);
              });
            });
      });
    } else {
      constraintViolations.forEach(violation -> {
        log.error(violation.getMessage());
      });
    }
  }

  private Optional<Element> findElementById(Document document, String targetElementId) {
    return Optional.ofNullable(document.getElementById(targetElementId));
  }

  private Optional<Elements> findElementsBySelector(Document document, String selector) {
    return Optional.ofNullable(document.select(selector));
  }

  private void findElementInTargetDocument(Element element, Document document) {
    Object[] attributes = element.attributes().asList().toArray();

    for (int k = attributes.length; k >= 1; k--) {
      if (find(attributes, 0, k, new Attribute[k], document)) {
        break;
      }
    }
  }


  private boolean find(Object[] attributes, int n, int k,
      Object[] combination, Document document) {
    if (k == 0) {
      String selector = new CSSSelectorBuilderImpl().buildSelector(Arrays.asList(combination));
      Optional<Elements> elementsOpt = findElementsBySelector(document, selector);
      if (elementsOpt.isPresent()) {
        Elements elements = elementsOpt.get();
        if (elements.size() == 1) {
          log.info("Matched attributes: {}", selector);
          log.info("Target element path: {}",
              new PathBuilderImpl().buildPathToElement(Iterables.getOnlyElement(elements)));
          return true;
        }
      }
    } else {
      for (int i = n; i <= attributes.length - k; i++) {
        combination[combination.length - k] = attributes[i];
        if (find(attributes, i + 1, k - 1, combination, document)) {
          return true;
        }
      }
    }
    return false;
  }

  private Optional<Document> getDocument(File htmlFile) {
    try {
      return Optional.of(Jsoup.parse(
          htmlFile,
          CHARSET_NAME,
          htmlFile.getAbsolutePath()));
    } catch (IOException e) {
      log.error("Error reading [{}] file", htmlFile.getAbsolutePath(), e);
      return Optional.empty();
    }
  }
}
