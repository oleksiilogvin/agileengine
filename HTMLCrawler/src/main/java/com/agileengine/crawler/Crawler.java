package com.agileengine.crawler;

import com.agileengine.context.Context;

public interface Crawler {

  void findElementPath(Context context);
}