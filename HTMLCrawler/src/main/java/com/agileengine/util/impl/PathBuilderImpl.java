package com.agileengine.util.impl;

import com.agileengine.util.PathBuilder;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.jsoup.nodes.Node;

public class PathBuilderImpl implements PathBuilder {

  private static final String PARENT_SELECTOR = " > ";
  private static final String ROOT_NODE = "html";

  @Override
  public String buildPathToElement(Node element) {
    StringBuilder builder = new StringBuilder();
    buildPath(element, builder);
    return builder.toString();
  }

  private void buildPath(Node element, StringBuilder builder) {
    addElement(element, builder);
    while (element.hasParent()) {
      element = element.parent();
      if (addElement(element, builder)) {
        break;
      }
    }
  }

  private boolean addElement(Node element, StringBuilder builder) {
    addIndex(builder, element);
    String nodeName = element.nodeName();
    builder.insert(0, nodeName);
    if (Objects.equals(nodeName, ROOT_NODE)) {
      return true;
    }
    builder.insert(0, PARENT_SELECTOR);
    return false;
  }

  private void addIndex(StringBuilder builder, Node element) {
    long index = getIndex(element);
    if (index > 0) {
      builder.insert(0, "[" + index + "]");
    }
  }

  private long getIndex(Node element) {
    List<Node> siblings = element.siblingNodes().stream().filter(
        sibling -> Objects.equals(sibling.nodeName(), element.nodeName()))
        .collect(Collectors.toList());

    for (int i = 0; i < siblings.size(); i++) {
      if (siblings.get(i).siblingIndex() > element.siblingIndex()) {
        return i - 1;
      }
    }
    return siblings.size();
  }
}
