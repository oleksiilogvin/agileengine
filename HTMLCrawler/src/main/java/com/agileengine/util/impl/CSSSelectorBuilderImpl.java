package com.agileengine.util.impl;

import com.agileengine.util.CSSSelectorBuilder;
import java.util.List;
import java.util.stream.Collectors;
import org.jsoup.nodes.Attribute;

public class CSSSelectorBuilderImpl implements CSSSelectorBuilder {

  public String buildSelector(List<Object> attributes) {
    return "a" + attributes.stream()
        .map(Attribute.class::cast)
        .map(attribute -> "[" + attribute.getKey() + "=" + attribute.getValue() + "]")
        .collect(Collectors.joining());
  }
}
