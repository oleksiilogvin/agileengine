package com.agileengine.util;

import org.jsoup.nodes.Node;

public interface PathBuilder {

  String buildPathToElement(Node element);
}
