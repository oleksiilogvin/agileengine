package com.agileengine.util;

import java.util.List;

public interface CSSSelectorBuilder {

  String buildSelector(List<Object> attributes);
}
