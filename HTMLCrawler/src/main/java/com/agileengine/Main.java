package com.agileengine;

import com.agileengine.context.Context;
import com.agileengine.context.ContextManager;
import com.agileengine.crawler.impl.CrawlerImpl;

public class Main {

  public static void main(String[] args) {
    Context context = new ContextManager().resolveContext(args);
    new CrawlerImpl().findElementPath(context);
  }
}
