package com.agileengine.context;

import java.util.Arrays;
import java.util.function.Consumer;
import java.util.regex.Pattern;

public class ContextManager {

  private static final Pattern TARGET_DOCUMENT = Pattern.compile("(.+\\.)(html)");
  private static final String SOURCE_DOCUMENT = "sample-0-origin.html";

  public Context resolveContext(String[] args) {
    Context context = new Context();
    Arrays.stream(args).forEach(resolveArgument(context));
    return context;
  }

  private Consumer<String> resolveArgument(Context context) {
    return arg -> {
      if (arg.endsWith(SOURCE_DOCUMENT)) {
        context.setSourceDocument(arg);
      } else if (TARGET_DOCUMENT.matcher(arg).matches()) {
        context.setTargetDocument(arg);
      } else {
        context.setElementId(arg);
      }
    };
  }
}
