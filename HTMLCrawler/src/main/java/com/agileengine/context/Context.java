package com.agileengine.context;

import javax.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class Context {

  private String elementId = "make-everything-ok-button";
  @NotBlank(message = "Path to source document should not be blank")
  private String sourceDocument;
  @NotBlank(message = "Path to target document should not be blank")
  private String targetDocument;
}
