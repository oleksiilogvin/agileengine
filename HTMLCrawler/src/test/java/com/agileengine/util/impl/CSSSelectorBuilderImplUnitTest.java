package com.agileengine.util.impl;

import static org.junit.Assert.assertEquals;

import com.agileengine.util.CSSSelectorBuilder;
import java.util.ArrayList;
import java.util.List;
import org.jsoup.nodes.Attribute;
import org.junit.Test;

public class CSSSelectorBuilderImplUnitTest {

  private CSSSelectorBuilder selectorBuilder = new CSSSelectorBuilderImpl();

  @Test
  public void shouldReturnCSSSelector() {
    final String key1 = "key1";
    final String value1 = "value1";
    final String key2 = "key2";
    final String value2 = "value2";
    String expectedSelector = "a[" + key1 + "=" + value1 + "][" + key2 + "=" + value2 + "]";
    List<Object> attributes = new ArrayList<>();
    attributes.add(new Attribute(key1, value1));
    attributes.add(new Attribute(key2, value2));

    String actualSelector = selectorBuilder.buildSelector(attributes);

    assertEquals(expectedSelector, actualSelector);
  }
}