package com.agileengine.util.impl;

import static org.junit.Assert.assertEquals;

import com.agileengine.util.PathBuilder;
import org.jsoup.nodes.Element;
import org.junit.Test;

public class PathBuilderImplUnitTest {

  private PathBuilder pathBuilder = new PathBuilderImpl();

  @Test
  public void shouldReturnPathToElement() {
    Element html = new Element("html");
    Element mainDiv = new Element("div");
    Element nestedDiv1 = new Element("div");
    Element nestedDiv2 = new Element("div");
    Element a = new Element("a");
    nestedDiv2.appendChild(a);
    mainDiv.appendChild(nestedDiv1);
    mainDiv.appendChild(nestedDiv2);
    html.appendChild(mainDiv);
    String expectedPath = "html > div > div[1] > a";

    String actualPath = pathBuilder.buildPathToElement(a);

    assertEquals(expectedPath, actualPath);
  }
}